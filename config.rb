require 'slim'

###
# Page options, layouts, aliases and proxies
###

# Per-page layout changes:
#
# With no layout
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

activate :directory_indexes
set :debug_assets, true

# With alternative layout
# page '/path/to/file.html', layout: :otherlayout

# Proxy pages (http://middlemanapp.com/basics/dynamic-pages/)
# proxy '/this-page-has-no-template.html', '/template-file.html', locals: {
#  which_fake_page: 'Rendering a fake page with a local variable' }

###
# Helpers
###

page "fleet/*", :layout => :fleet_layout
page "fleet.html", :layout => :fleet_layout
# page "booknow.html", :layout => :empty_layout
page "/404.html", directory_index: false

data.fleet.car.each do |car|
  proxy "/fleet/#{car.slug}.html", "/fleet/fleet-detail.html", 
  :locals => { :car => car }, 
  :ignore => true
end

page "services/*", :layout => :services_layout
page "services.html", :layout => :services_layout

data.services.service.each do |serve|
  proxy "/services/#{serve.slug}.html", "/services/services-detail.html", 
  :locals => { :serve => serve }, 
  :ignore => true
end

activate :blog do |blog|
  # set options on blog
  blog.layout = "blog_layout"
  blog.prefix = "blog"
  blog.permalink = "{title}.html"
  blog.new_article_template = "blog/article.html.markdown.slim"
end

activate :ogp do |ogp|
  #
  # register namespace with default options
  #
  ogp.namespaces = {
    fb: data.ogp.fb,
    # from data/ogp/fb.yml
    og: data.ogp.og
    # from data/ogp/og.yml
  }
  ogp.base_url = 'http://myfdtps.com'
end

# Reload the browser automatically whenever files change
configure :development do
  activate :livereload
end

# Build-specific configuration
configure :build do

  # Minify CSS on build
  activate :minify_css

  # Minify Javascript on build
  activate :minify_javascript

  # Minify HTML on build
  activate :minify_html

  # activate :imageoptim
  set :relative_links, false
  activate :relative_assets
  set :strip_index_file, true
end
