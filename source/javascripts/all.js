//= require_tree .

var js = js || {},
  body = document.getElementsByTagName('body')[0];

// Scripts
js.main = {
  init: function () {
    this.cookiesAlert();
    this.drift();
    this.fullstory();
    // this.barba();
    this.externalLinks();
    this.menu();
    this.ajaxForm();
    this.toggleVisibility();
  },
  ajaxForm: function () {
    var $form = $(".site-form-careers");
    $form.validate({
      // Specify validation rules
      rules: {
        contact_email: {
          required: true,
          // Specify that email should be validated
          // by the built-in "email" rule
          email: true
        },
        password: {
          required: true,
          minlength: 5
        },
        signature_name: "required",
        signature_name_confirmed: {
          equalTo: "#signature_name"
        },
        signature_date: "required"
      },
      // Specify validation error messages
      messages: {
        job_position: "Please select a position",
        personal_name_first: "Please enter your first name",
        personal_name_last: "Please enter your last name",
        personal_license: "Please enter your Driver's License Number",
        personal_license_state: "Please selct the state for your Driver's License",
        password: {
          required: "Please provide a password",
          minlength: "Your password must be at least 5 characters long"
        },
        email: "Please enter a valid email address",
        signature_name: "Please Type Your Full Name",
        signature_name_confirmed: "Please Confirm Your Full Name",
        signature_date: "Please add today's date"
      },
      // Make sure the form is submitted to the destination defined
      // in the "action" attribute of the form when valid
      submitHandler: function(form) {
          $.post($form.attr("action"), $form.serialize()).then(function(){
            window.location.href = "/careers/thankyou-careers";
          });
      }
    });
    
  },
  barba: function() {
    var FadeTransition = Barba.BaseTransition.extend({
      start: function() {
        /**
         * This function is automatically called as soon the Transition starts
         * this.newContainerLoading is a Promise for the loading of the new container
         * (Barba.js also comes with an handy Promise polyfill!)
         */
    
        // As soon the loading is finished and the old page is faded out, let's fade the new page
        Promise
          .all([this.newContainerLoading, this.fadeOut()])
          .then(this.fadeIn.bind(this));
      },
    
      fadeOut: function() {
        /**
         * this.oldContainer is the HTMLElement of the old Container
         */
    
        return $(this.oldContainer).animate({ opacity: 1 }).promise();
        
      },
    
      fadeIn: function() {
        /**
         * this.newContainer is the HTMLElement of the new Container
         * At this stage newContainer is on the DOM (inside our #barba-container and with visibility: hidden)
         * Please note, newContainer is available just after newContainerLoading is resolved!
         */
        var _this = this;
        var $el = $(this.newContainer);
        
        $(this.oldContainer).hide();
        
        $el.css({
          visibility : 'visible',
          opacity : 1
        });
        
        $el.animate({ opacity: 1 }, 800, function() {
          /**
           * Do not forget to call .done() as soon your transition is finished!
           * .done() will automatically remove from the DOM the old Container
           */
          _this.done();
        });
      },

    });
    
    /**
     * Next step, you have to tell Barba to use the new Transition
     */
    
    Barba.Pjax.getTransition = function() {
      /**
       * Here you can use your own logic!
       * For example you can use different Transition based on the current page or link...
       */
      return FadeTransition;
    };
    var home = Barba.BaseView.extend({
      namespace: 'home',
      onEnter: function() {
        $('body').addClass('menu-trans-out');
        $('.fader-home').addClass('on').removeClass('hide');
        setTimeout(function(){
          if($('body').hasClass('menu-active')){
            $('body').removeClass('menu-active');
          }
          $('.nav-mobile').removeClass('active');
        }, 200);
        setTimeout(function(){
          $('.fader-home').addClass('hide').removeClass('on');
        }, 1000);
        setTimeout(function(){
          $('.fader-home').removeClass('hide');
          $('body').removeClass('menu-trans-out');
        }, 2000);
        return false;
      },
      onEnterCompleted: function() {
        
      },
      onLeave: function() {
          // A new Transition toward a new page has just started.
        
      },
      onLeaveCompleted: function() {
          // The Container has just been removed from the DOM.
        
      }
    });
    var booknow = Barba.BaseView.extend({
      namespace: 'booknow',
      onEnter: function() {
        setTimeout(function(){
          document.getElementById('booknow_iframe').innerHTML = '<a href="https://book.mylimobiz.com/v4/foundtrans" data-ores-widget="website" data-ores-alias="foundtrans">Online Reservations</a><script type="text/javascript" src="https://book.mylimobiz.com/v4/widgets/widget-loader.js"></script>';
        }, 2000);
        return false;
      },
      onEnterCompleted: function() {
        
      },
      onLeave: function() {
          // A new Transition toward a new page has just started.
        
      },
      onLeaveCompleted: function() {
          // The Container has just been removed from the DOM.
        
      }
    });
    
    Barba.Dispatcher.on('linkClicked', function(currentStatus, oldStatus, container) {
      $('.site-main').addClass('out');
      setTimeout(function(){
        if($('body').hasClass('menu-active')){
          $('body').removeClass('menu-active');
          $('body').addClass('menu-trans-out');
        }
        $('.nav-mobile').removeClass('active');
        $('.fader').addClass('on').removeClass('hide');
      }, 200);
      setTimeout(function(){
        $('.site-main').addClass('out');
      }, 250);
    });
    Barba.Dispatcher.on('transitionCompleted', function(currentStatus, oldStatus, container) {
      setTimeout(function(){
        $('.fader').addClass('hide').removeClass('on');
      }, 400);
      setTimeout(function(){
        $(window).scrollTop(0);
      }, 500);
      setTimeout(function(){
        $('.fader').removeClass('hide');
      }, 800);
      setTimeout(function(){
        $('.site-main').removeClass('out');
        $('body').removeClass('menu-trans-out');
      }, 900);
      return false;
    });

    // Don't forget to init the view!
    // home.init();
    booknow.init();
    Barba.Prefetch.init();
    Barba.Pjax.start();
  },
  cookiesAlert: function() {
    window.addEventListener("load", function(){
      window.cookieconsent.initialise({
        "content": {
          "message": "This website uses cookies to ensure you get the best experience on our website.",
          "href": "http://myfdtps.netlify.com/privacy-policy"
        }
      })});
  },
  drift: function() {
    "use strict";

      !function() {
        var t = window.driftt = window.drift = window.driftt || [];
        if (!t.init) {
          if (t.invoked) return void (window.console && console.error && console.error("Drift snippet included twice."));
          t.invoked = !0, t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ], 
          t.factory = function(e) {
            return function() {
              var n = Array.prototype.slice.call(arguments);
              return n.unshift(e), t.push(n), t;
            };
          }, t.methods.forEach(function(e) {
            t[e] = t.factory(e);
          }), t.load = function(t) {
            var e = 3e5, n = Math.ceil(new Date() / e) * e, o = document.createElement("script");
            o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + n + "/" + t + ".js";
            var i = document.getElementsByTagName("script")[0];
            i.parentNode.insertBefore(o, i);
          };
        }
      }();
      drift.SNIPPET_VERSION = '0.3.1';
      drift.load('cfa5c6z5eibe');
  },
  externalLinks: function() {
    function externalLinks() {
      var anchors = document.querySelectorAll( 'a' );
      for( var i = 0; i < anchors.length; i++ ) {
        if ( anchors[i].hostname !== window.location.hostname ) {
            anchors[i].setAttribute( 'target', '_blank' );
        }
      }
    }
    externalLinks();
  },
  fullstory: function() {
    window['_fs_debug'] = false;
    window['_fs_host'] = 'fullstory.com';
    window['_fs_org'] = 'DK66X';
    window['_fs_namespace'] = 'FS';
    (function(m,n,e,t,l,o,g,y){
        if (e in m) {if(m.console && m.console.log) { m.console.log('FullStory namespace conflict. Please set window["_fs_namespace"].');} return;}
        g=m[e]=function(a,b,s){g.q?g.q.push([a,b,s]):g._api(a,b,s);};g.q=[];
        o=n.createElement(t);o.async=1;o.src='https://'+_fs_host+'/s/fs.js';
        y=n.getElementsByTagName(t)[0];y.parentNode.insertBefore(o,y);
        g.identify=function(i,v,s){g(l,{uid:i},s);if(v)g(l,v,s)};g.setUserVars=function(v,s){g(l,v,s)};g.event=function(i,v,s){g('event',{n:i,p:v},s)};
        g.shutdown=function(){g("rec",!1)};g.restart=function(){g("rec",!0)};
        g.consent=function(a){g("consent",!arguments.length||a)};
        g.identifyAccount=function(i,v){o='account';v=v||{};v.acctId=i;g(o,v)};
        g.clearUserCookie=function(){};
    })(window,document,window['_fs_namespace'],'script','user');
  },
  menu: function() {
    var body = document.querySelector('body');
    var menuTrig = document.querySelector('#menuTrig');
    var menu = document.querySelector('#menu');
    var menuClose = document.querySelector('#menuClose');
    // var cd = document.querySelector('.content-calendar-current-date');

    menuTrig.onclick = function() {
      if (menu.classList.contains('active')){
        body.classList.add('menu-trans-out');
        body.classList.remove('menu-active');
        setTimeout(function(){
          menu.classList.remove('active');
          body.classList.remove('menu-trans-out');
        }, 500);
      }
      else {
        menu.classList.add('active');
        body.classList.add('menu-trans');
        setTimeout(function(){
          body.classList.add('menu-active');
          body.classList.remove('menu-trans');
        }, 500);
        
      } 
    };
    menuClose.onclick = function() {
      body.classList.add('menu-trans-out');
      body.classList.remove('menu-active');
      setTimeout(function(){
        menu.classList.remove('active');
        body.classList.remove('menu-trans-out');
      }, 500);
    }
  },
  toggleVisibility: function() {
    $('.toggle-trigger').on('click', function(){
      var $s = $(this).closest('.toggle-parent').find('.toggle-subject');
      if($s.hasClass('hidden')){
        $s.removeClass('hidden');
        $(this).text('Hide');
      } else {
        $s.addClass('hidden');
        $(this).text('Show');
      }
    });
  },
};
document.addEventListener('DOMContentLoaded', function(){
  js.main.init();
});